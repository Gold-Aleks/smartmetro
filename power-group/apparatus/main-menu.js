let mainMenu = `<input type="checkbox" id="hmt" class="hidden-menu-ticker">

<label class="btn-menu" for="hmt">
  <span class="first"></span>
  <span class="second"></span>
  <span class="third"></span>
</label>

<ul class="hidden-menu">
  <li><a href="../../index.html">ГЛАВНАЯ</a></li>  
  <li><a href="../../power.html">СИЛОВАЯ СХЕМА</a></li>
  <li><a href="../../control.html">СХЕМА УПРАВЛЕНИЯ</a></li>
  <li><a href="../../apparatus.html">ЭЛЕКТРОАППАРАТЫ</a></li>
  <li><a href="../../extrim.html">Пульт 81.717 ЛОТ</a></li>  
  <li><a href="../../extrim-kvr.html">Пульт КВР</a></li>
  <li><a href="../../test.html">НЕСТАНДАРТНЫЕ ПОКАЗАНИЯ ЛОТ</a></li>  
</ul>`;

document.querySelector('#main-menu').innerHTML = mainMenu;