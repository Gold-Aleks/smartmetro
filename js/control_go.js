 let hind12 = document.querySelector('#hind12'); //кнопка реверс вперед
let hind13 = document.querySelector('#hind13'); //кнопка вкл АРС
let hind14 = document.querySelector('#hind14'); //КМ - Ход-1, запитка У2, вкл красной РП
let hind15 = document.querySelector('#hind15'); 
let vz2 = document.querySelector('#vz2');//катушка ВЗ-2
let kontRpb39 = document.querySelector('#kont-rpb39');//конт РПБ пров39
let bodyRPB = document.querySelector('#body-rpb'); //катушка РПБ
let rpRed = document.querySelector('#rp-red');
let bodyLk2 = document.querySelector('#body-lk2');
let bodyRv2 = document.querySelector('#body-rv2');

// let but = document.querySelector('.but');
// let butClose = document.querySelector('.menu__drop');
// for(let i = 0; i < butClose.length; i++ ){
//     butClose[i].onclick = function(){
//         but.classList.add('slip');
//     }
// }

let preGo = document.querySelector('#pre-go'); //показать окно, скрыть меню
preGo.onclick = () =>{
    but.style.display="none";
    hind12.style.display="block";
}

let bot12 = document.querySelector('#bot12'); // запитать ВЗ-2 по проводу 39
bot12.onclick = () =>{
    pow39.play(function(){ // КОНСТРУКЦИЯ ПОЗВОЛЯЮЩАЯ ВЫЗЫВАТЬ ФУНКЦИЮ ПОСЛЕ ЗАВЕРШЕНИЯ АНИМАЦИИ!!!
        hind13.style.display="block";
        vz2.classList.add('st57');
    });
    but.style.display="none";
    hind12.style.display="none";
    kontRpb39.classList.add('st99');
}

let bot13 = document.querySelector('#bot13'); //запитать РПБ
bot13.onclick = () =>{  // поклику мод13 запускает запитку АРС
    $('#lot').html(lot); //пульт ЛОТ
    onAPC.play ( function(){
        pow39.reset();      //запитка 39го провода
        vz2.classList.remove('st57'); // заливка катушки ВЗ2
        kontRpb39.style.display="none"; // скрывает обратный контакт РПБ
        $('#body-rpb').addClass('st57');  // заливка катушки РПБ
       // hind14.style.display="block";   // скрыть модальное
       $('#l40,#SN,#RIP,#UG,#zero-right,#zero-left').css({"opacity" : 1}); // ЛОТ загораются лампочки на пульте 40
		});
    hind13.style.display="none";
}
////////// ХОД-1
//let go1Cont = document.querySelector('#go1-but'); // Ход1 запитка У2, вкл лампы РП
$('#go1-but').click(function(){
    but.style.display="none";
    $('#hind14').show()
})
   
$('#bot14').click(function(){
    go1Pow();
})

let bot14 = document.querySelector('#bot14');//окно с кнопкой Ход1

function go1Pow(){// функция Ход1
  powU2.play( function(){  //запитка У-2
   rpRed.classList.add('st57');
    $('#hind15').show();
  });
  $('.but, #hind14').hide();  // скрывает модальное Ход1 и меню
}

let bot15 = document.querySelector('#bot15');//окно с кнопкой вкл РВ2
bot15.onclick = () =>{
hind15.style.display="none";
   powU2.reset();
powRv2.play( function(){
    bodyRv2.classList.add('st57');// заливка катушки РВ2
    hind16.style.display="block";
 });
}

let bot16 = document.querySelector('#bot16');//окно с кнопкой вкл ЛК2
bot16.onclick = () =>{
    hind16.style.display="none";
    powRv2.reset();
    powLk2.play(function(){
         bodyLk2.classList.add('st57');// заливка катушки ЛК2
         hind17.style.display="block";
         $('#lk2-pr1').css('display','block');
    });
   
}

let bot17 = document.querySelector('#bot17');//окно с кнопкой вкл Р1-5
bot17.onclick = () =>{
 powLk2.reset();
 powP1_5.play(function(){
  $('#body-ksh1, #body-ksh2').addClass('st57');
  $('#ksh2-pr1').css('display','block'); 
  hind18.style.display="block"; 
 });
 hind17.style.display="none";
 $('#bodyP1-5').addClass('st57');  // заливка катушки P1-5
}

let bot18 = document.querySelector('#bot18');//окно переключения ПМ
bot18.onclick = ()=>{
    powPM.play(function(){
        $('#body-pm').addClass('st57');
        hind19.style.display="block"; 
        $('#pmu2-pr1, #pmu-pr1').css('display','block'); 
    });
    hind18.style.display="none"; 
    name.innerHTML = 'Переключение в моторный режим';
}
 
let bot19 = document.querySelector('#bot19');//окно вкл ЛК
bot19.onclick = ()=>{
    pow1.play(function(){
        $('#body-lk1, #body-lk3, #body-lk4, #body-lk5').addClass('st57');
        $('#kont-lk4-prov24').css('display','none');
        rpRed.classList.remove('st57');
        powPM.reset();
    });
    hind19.style.display="none"; 
    name.innerHTML = 'Включение всех ЛК, сбор схемы на "Ход"';
}


// bot14.onclick = ()=>{ // функция вкл Ход1 продолжение АРС от модального
//     go1Pow();
// }
// go1But.onclick = ()=>{ // функция вкл Ход1 чистый ход от меню
//     go1Pow();
// }



//функция - начитка голосом
// const message = new SpeechSynthesisUtterance(talkText); 
 function talk(){
    let synth = window.speechSynthesis,
    message = new SpeechSynthesisUtterance();
    message.lang = 'ru';
    message.text = talkText;
    message.rate = '2';
    message.pitch ='1.2';
    synth.speak(message);
   }

let ex = document.querySelector('#u2-rp-red');


// функция - останавливает голос по клику на динамик и убирает картинку динамика
let sound = document.querySelector('#sound');
sound.onclick = ()=>{
//ex.style.opacity='.5'; // вынести в функцию включающую ЛК2
sound.src='';
speechSynthesis.cancel();
}

////////////////// TOPMP3



let t1 = document.querySelector('#t1');
t1.onclick = ()=>{
    $('#hind4').show();
    $('.but').toggle();
}

let bot4 = document.querySelector('#bot4');
    bot4.onclick = () =>{ 
        $('#hind4').slideUp(300);   
    powU2.play( function(){  //запитка У-2
        rpRed.classList.add('st57');
         hind20.style.display="block";
         $('#body-rvt').addClass('st57');
       });
     
   $('#kontroler-20,#kont-rpb').hide();
   $('#rvt').toggle();//включаем РВТ из переменной ход1 (const u2RpRedCode)
   
}

let bot20 = document.querySelector('#bot20');//окно с кнопкой вкл ЛК2
bot20.onclick = () =>{
    $('#hind20').slideUp(300);
    powLk2.play(function(){
         bodyLk2.classList.add('st57');// заливка катушки ЛК2
         $('#body-k6').addClass('st57');
         $('#hind21').css("display","block");
    });
    $('#kontroler-20t, #pr10-k6').show();
    $('#kontroler-20').hide();
    $('#lk2-pr1').css('display','block');
  
}

let bot21 = document.querySelector('#bot21');//
bot21.onclick = () =>{
    $('#hind21').slideUp(300);
    prov6.play(function(){
        //  bodyLk2.classList.add('st57');// заливка катушки ЛК2
         $('#body-tr').addClass('st57');
         $('#body-ksb1').addClass('st57');
         $('#body-ksb2').addClass('st57');
         $('#hind22').css("display","block");
    });

}

let bot22 = document.querySelector('#bot22');//
bot22.onclick = () =>{
    $('#hind22').slideUp(300);
    tormPow.play(function(){
        rpRed.classList.remove('st57');
        powU2.reset();
        $('#body-lk3, #body-lk4').addClass('st57');
        $('#kont-lk4-prov24').css('display','none');
        rpRed.classList.remove('st57');
    });

}