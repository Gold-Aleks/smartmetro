// let notVis1 = '';
// let notVis2 = '';
// let notVis3 = '';
// let notVis4 = '';
// let notVis5 = '';

// function clearFull(){
// 	notVis1.stop();
// 	notVis1.reset();
// 	notVis2.stop();
// 	notVis2.reset();
// 	notVis3.stop();
// 	notVis3.reset();
// 	notVis4.stop();
// 	notVis4.reset();
// 	notVis5.stop();
// 	notVis5.reset();
// 	$('.st53').removeClass('st57');
// }

menu.onclick = menuReset; //перезагрузка
function menuReset(){
document.location.reload(true); //при клике на меню перегружает страницу
 but.style.display="block"; // показать выпадающее меню
 //clearFull()
 }
	
	
//  мотор-компрессор
document.querySelector('#mk').onclick = mk1; //получил кнопку компрессор и записал в функцию
function mk1(){  
mk.play(); // функция vivus 
name.innerHTML = 'Работа компрессора'; //записать при клике название вызваной схемы
but.style.display="none"; //скрытие меню
//notVis1 = mk;
 }
// резервный мотор-компрессор
document.querySelector('#rmk').onclick = rmk1; //получил кнопку резервный компрессор и записал в функцию
function rmk1(){ 
rmk.play(); // функция vivus 
name.innerHTML = 'Резервный мотор-компрессор'; //записать при клике название вызваной схемы
but.style.display="none"; //скрытие меню
//notVis1 = rmk;
}
// хвосты

document.querySelector('#tail-but').onclick = tailLamp; 
function tailLamp(){  
tail.play(); // функция vivus 
name.innerHTML = 'Включение хвостовых огней'; 
but.style.display="none";
document.querySelector('#tail-r').classList.add('st57');
document.querySelector('#tail-l').classList.add('st57');
//notVis1 = tail;
}
// открытие дверей
document.querySelector('#door').onclick = door;
function door(){
name.innerHTML = 'Открытие дверей';
hind1.style.display ='block';
but.style.display="none";
}
// запитка 10го провода
document.querySelector('#bot1').onclick = hindOne;
function hindOne(){
pr.play(function(){
	hind2.style.display ='block';
}); // функция vivus 
hind1.style.display ='none';
//notVis1 = pr;
}
// подведение питания к кнопкам открытия
document.querySelector('#bot2').onclick = hindTwo;
function hindTwo(){
lamp.play(function(){
	hind3.style.display ='block';
}); // функция vivus 
hind2.style.display ='none';
//notVis2 = lamp;
}
//питание на ДВР
document.querySelector('#bot3').onclick = hindThree;
function hindThree(){
OpenLeft.play()
hind3.style.display ='none';
//notVis3 = OpenLeft;
}
// // объяснение+выход
// let hindFour = document.querySelector('#bot4');
// hindFour.onclick = () =>{
// 	hind4.style.display ='none';
	
// menuReset();
// }

// переменные не убирать отсюда!
let white1 = document.querySelector('#white-lamp1'); //белая кузовная лампа 
let white2 = document.querySelector('#white-lamp2'); //белая кузовная лампа
let pr16 = document.querySelector('#prov16');  // 16 провод
let rdClose = document.querySelector('#rd-close'); //контакт РД в цепи земли ДВР1
let releRd = document.querySelector('#rd-rele');// катушка РД

document.querySelector('#door-cl').onclick = doorCl;
function doorCl(){
 doorClose.play( function(){
	white1.style.display="none";
	white2.style.display="none";
    pr16.style.display="none";
	rdClose.style.display="none";
	releRd.classList.add('st57');
	}); // функция vivus 
 name.innerHTML = 'Закрытие дверей';
but.style.display="none";
rdClose.classList.add('st101');
//notVis1 = doorClose;
}

document.querySelector('#door-rez-but').onclick = doorRezF;
function doorRezF(){
 doorRez.play(); // функция vivus 
 name.innerHTML = 'Резервное закрытие дверей'; 
 but.style.display="none";
//openWind = true;
//notVis1 = doorRez;
}

// цепь ЛСД /////////////////////////////////////////////////
document.querySelector('#door-lsd').onclick = signPower;
function signPower(){
	$('#rd-close').toggle(false)
	hind7.style.display="block";
	but.style.display="none";
	rdPow.style.opacity=1;  //цепь питания РД
	 kontRd.style.display="block"; //показать контакт РД в цепи ЛСД
	releRd.classList.add('st57');  //залить катушку РД
}
document.querySelector('#bot7').onclick = sign; //получил кнопку и записал в функцию
function sign(){  
signDoorHead.play(function(){
hind8.style.display ='block';
}); // функция vivus 
name.innerHTML = 'Головной вагон'; 
but.style.display="none";
hind7.style.display ='none';
//openWind = true;
//notVis1 = signDoorHead;
}
document.querySelector('#bot8').onclick = signV; //получил кнопку и записал в функцию
function signV(){  
signDoorVagon.play(function(){
	hind9.style.display ='block';
}); // функция vivus 
name.innerHTML = 'Промежуточные вагоны'; 
signDoorHead.reset(),signDoorHead.stop()//,doorClose.reset(),doorClose.stop();
hind8.style.display ='none';
//openWind = true;
}
document.querySelector('#bot9').onclick = signT;
function signT(){
	signDoorTail.play(function(){
		hind10.style.display ='block';
	})
	name.innerHTML = 'Хвостовой вагон';
	signDoorVagon.reset();
	hind9.style.display ='none';
}
document.querySelector('#bot10').onclick = signLSD;
function signLSD(){
	signDoorLSD.play()
	name.innerHTML = 'Включение ЛСД';
	document.querySelector('#kd').classList.add('st57');
	hind10.style.display="none";
		//openWind = false;
}
///////////////////////////////////////////////////////////

document.querySelector('#kvc-but').onclick = kvcPow; //получил кнопку и записал в функцию
function kvcPow(){  
kvc.play(function(){
	hind5.style.display ='block';// окно-подсказка
}); // функция vivus 
name.innerHTML = 'Контактор вспомогательных цепей'; 
but.style.display="none";
openWind = true;
notVis1 = kvc;
}

let kvcS = document.querySelector('#kvc'); 
document.querySelector('#bot5').onclick = A53;
function A53(){
kvcOn.play(function(){
	kvcS.style.display="none";
	bodyKvc.classList.add('st57');
}); // функция vivus 
hind5.style.display ='none';
// setTimeout( function(){

// },8000)
// openWind = false;
}

document.querySelector('#war-but').onclick = warPow; //получил кнопку и записал в функцию
function warPow(){  
war.play(); // функция vivus 
name.innerHTML = 'Отопление кабины'; 
but.style.display="none";
document.querySelector('#kup').classList.add('st57');
document.querySelector('#stove').classList.add('st57');
openWind = true;
}

document.querySelector('#avar-but').onclick = avarPow; //получил кнопку и записал в функцию
function avarPow(){  
avar.play(); // функция vivus 
name.innerHTML = 'Аварийное освещение салона'; 
but.style.display="none";
openWind = true;
}

document.querySelector('#far-but').onclick = farPow; //получил кнопку и записал в функцию
function farPow(){  
fars.play(); // функция vivus 
name.innerHTML = 'Фары'; 
hind6.style.display ='block';
but.style.display="none";
openWind = true;
}

document.querySelector('#bot6').onclick = far2; //получил кнопку и записал в функцию
function far2(){  
fars2.play(); // функция vivus 
name.innerHTML = 'Усиленный свет'; 
but.style.display="none";
hind6.style.display ='none';
openWind = true;
}
////////////////// реверс /////////////////////////////////


let revModal = document.querySelector('#rev-but');
 revModal.onclick = () =>{
	hind11.style.display ='block';
	but.style.display="none";
	//kontRevV1.classList.add('st101');
	//kontRevV2.classList.add('st101');
}

let revV = document.querySelector('#bot11-v');
revV.onclick = () =>{
	name.innerHTML = 'Реверс вперёд';
	$('#rev-back').addClass('st57');
	kontRevV1.style.display="none";
	kontRevV2.style.display="none";
	kontRevN1.style.display="block";
	kontRevN2.style.display="block";
	hind11.style.display ='none';
	revVp.play(function(){
		kontRevV1.style.display="block"
		kontRevV2.style.display="block"
		kontRevV1.classList.add('st101');
		kontRevV2.classList.add('st101');
		$('#body-rkr').addClass('st57');
		document.querySelector('#rev-up').classList.add('st57');
		document.querySelector('#rev-back').classList.remove('st57');
		document.querySelector('#rev-up-pow').style.display="none";
		kontRevN1.style.display="none";
		kontRevN2.style.display="none";
		})	
}
let revN = document.querySelector('#bot11-n');
revN.onclick = () =>{
	name.innerHTML = 'Реверс назад';
	$('#rev-up').addClass('st57');
	kontRevV1.style.display="block"
		kontRevV2.style.display="block"
		kontRevV1.classList.add('st101');
		kontRevV2.classList.add('st101');
	kontRevN1.style.display="none";
	kontRevN2.style.display="none";
	hind11.style.display ='none';
	revNz.play(function(){
		document.querySelector('#rev-back').classList.add('st57');
		document.querySelector('#rev-up').classList.remove('st57');
		document.querySelector('#rev-back-pow').style.opacity= 0;
		kontRevN1.style.display="block";
		kontRevN2.style.display="block";
		kontRevV1.style.display="none";
		kontRevV2.style.display="none";
		})	
}










