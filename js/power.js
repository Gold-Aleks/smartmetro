

let pp = document.querySelectorAll('.pp'); //паралельное соединение две линии
let ex = document.querySelector('#ex');	 // кнопка рк
let pk = document.querySelectorAll('.pk'); // позиции хода рк
let clicks = 1; //задан счетчик
let ish = document.querySelector('#pp-ish');
const ps12 = document.querySelector('#ps12');

function ps_pp(){
	$("#ps12").removeClass('st27');
	$("#ps12").addClass('st10');
}

function ishOn() {
	ish.style.display ='block'; 
 }
 
 function showEX() {
	ex.style.display ='inline'; // появляется кнопка РК
}

   ex.onclick = function(){
	resetHv(); // сброс предыдущих отрисовок
		   clicks += 1;
		if(clicks == 37){
			ex.style.display = 'none';
		}
        ex.innerHTML = clicks; // добавить счетчик внутрь кнопки
	for(let j=0; j < pp.length; j++){ // выбирает весь класс рp
	for(let i=0; i < pk.length; i++){ // выбирает весь класс рк
		switch(clicks){
	case 1:
		pk[0].style.display = 'block';
		break;
	case 2:
		pk[1].style.display = 'block';
		pk[0].style.display = 'none';
		break;
	case 3:
		pk[2].style.display = 'block';
		pk[1].style.display = 'none';	
	    break;
	case 4:
		pk[3].style.display = 'block';
		pk[2].style.display = 'none';
	    break;
	case 5:
		pk[4].style.display = 'block';
		pk[3].style.display = 'none';
	    break;	
	case 6:
		pk[5].style.display = 'block';
		pk[4].style.display = 'none';
	    break;	
	case 7:
		pk[6].style.display = 'block';
		pk[5].style.display = 'none';
	    break;	
	case 8:
		pk[7].style.display = 'block';
		pk[6].style.display = 'none';
		break;
	case 9:
		pk[8].style.display = 'block';
		pk[7].style.display = 'none';
		break;	
	case 10:
		pk[9].style.display = 'block';
		pk[8].style.display = 'none';
		break;	
	case 11:
		pk[10].style.display = 'block';
		pk[9].style.display = 'none';
		break;	
	case 12:
		pk[11].style.display = 'block';
		pk[10].style.display = 'none';
		break;	
	case 13:
		pk[12].style.display = 'block';
		pk[11].style.display = 'none';
		break;
	case 14:
		pk[13].style.display = 'block';
		pk[12].style.display = 'none';
		break;
	case 15:
		pk[14].style.display = 'block';
		pk[13].style.display = 'none';
		break;
	case 16:
		pk[15].style.display = 'block';
		pk[14].style.display = 'none';
		break;
	case 17:
		pk[16].style.display = 'block';
		pk[15].style.display = 'none';
		break;
	case 18:
		pk[16].style.display = 'block';
		pk[15].style.display = 'none';
		break;
     case 19:
		pk[17].style.display = 'none';
		pk[16].style.display = 'none';
		pp[1].style.display = "block";
		 break;
	case 20:
			pp[1].style.display = 'block';
		    break;
	case 21:
			pp[1].style.display = 'none';
			pp[2].style.display = "block";
			 break;
    case 22:
		    pp[2].style.display = 'none';
			pp[3].style.display = "block";
				 break;
    case 23:
			pp[3].style.display = 'none';
			pp[4].style.display = "block";
			 break;
    case 24:
			 pp[4].style.display = 'none';
			 pp[5].style.display = "block";
			  break;
    case 25:
			  pp[5].style.display = 'none';
			  pp[6].style.display = "block";
			   break;
     case 26:
			   pp[6].style.display = 'none';
			   pp[7].style.display = "block";
				break;
	case 27:
					pp[7].style.display = 'none';
					pp[8].style.display = "block";
					 break;
	case 28:
					 pp[8].style.display = 'none';
					 pp[9].style.display = "block";
					  break;
	case 29:
					pp[9].style.display = 'none';
					pp[10].style.display = "block";
					 break;
	case 30:
					pp[10].style.display = 'none';
					pp[11].style.display = "block";
					 break;
	case 31:
					pp[11].style.display = 'none';
					pp[12].style.display = "block";
					 break;
	case 32:
					pp[12].style.display = 'none';
					pp[13].style.display = "block";
					setTimeout(ishOn, 1500);  // на 32й позиции включаются КШ через время
					 
					 break;
	case 33:
					pp[13].style.display = 'none';
					pp[14].style.display = "block";
					ish.style.display ='none'; //убираем предыдущую отрисовку КШ
					 break;
	case 34:
					pp[14].style.display = 'none';
					pp[15].style.display = "block";
					 break;
	case 35:
						pp[15].style.display = 'none';
						pp[16].style.display = "block";
						 break;
    case 36:
							pp[16].style.display = 'block';
							//pp[17].style.display = "block";
							 break;
	case 37:
		document.location.reload(true);
					
					break;
	
	}		
	}	
	}		
    };

//////////////////////////////////////////////////////////////////////////
const rev = new Vivus(
        'rev1',
        {
			type: 'scenario',
            start: 'manual'
        }
        );



const go1 = new Vivus(
        'hv1',
        {
			type: 'scenario',
            start: 'manual'
        },showEX
        );

const ps = new Vivus(
        'ps',
        {
            type: 'scenario',
            start: 'manual'
        },ps_pp
        );

const go3 = new Vivus(
        'hod3',
        {
            type: 'scenario',
            start: 'manual'
        }
        );

const t1 = new Vivus(
		'torm1_1-3',
        {
            type: 'scenario',
            start: 'manual'
        }
		);
		

	
 

let r; //переменная изменяет динамически playHv()

 document.querySelector('#x1').onclick = x1; //получил кнопку ход1 и записал в функцию
 
 function x1(){  
	 clicks = 1;
    r = go1; // функция, при вызове записуем в r=go1 меняя работу function playHv на экземп.класса go1
	document.querySelector('.name').innerHTML = 'Ход 1 вперед'; //записать при клике название вызваной схемы
	rev.stop(),rev.reset(),ps.stop(),ps.reset(),go3.stop(),go3.reset(),t1.stop(),t1.reset(),go1.stop(),go1.reset()  //сброс при переключении схемы
	ex.style.display ='none';
 }

 document.querySelector('#x1n').onclick = x1n;
 function x1n(){
    r = rev;
	document.querySelector('.name').innerHTML = 'Ход 1 назад';
	go1.stop(),go1.reset(),ps.stop(),ps.reset(),go3.stop(),go3.reset(),t1.stop(),t1.reset(),rev.stop(),rev.reset()  //сброс при переключении схемы
	ex.style.display ='none'; // убрать кнопку рк при переключении
 }



 document.querySelector('#psbtn').onclick = psbtn;
 function psbtn(){
    r = ps;
	document.querySelector('.name').innerHTML = 'Переход с ПС на ПП';
	go1.stop(),go1.reset(),rev.stop(),rev.reset(),go3.stop(),go3.reset(),t1.stop(),t1.reset(),ps.stop(),ps.reset() //сброс при переключении схемы
	ex.style.display ='none'; // убрать кнопку рк при переключении
	$("#ps12").addClass('st27');
 }
document.querySelector('#x3').onclick = x3;
 function x3(){
    r = go3;
	document.querySelector('.name').innerHTML = 'Ход 3';
	go1.stop(),go1.reset(),ps.stop(),ps.reset(),rev.stop(),rev.reset(),t1.stop(),t1.reset(),go3.stop(),go3.reset() //сброс при переключении схемы
	ex.style.display ='none'; // убрать кнопку рк при переключении
 }

 document.querySelector('#t1').onclick = torm1;
 function torm1(){
    r = t1;
	document.querySelector('.name').innerHTML = 'Тормоз 1';
	go1.stop(),go1.reset(),ps.stop(),ps.reset(),rev.stop(),rev.reset(),go3.stop(),go3.reset(),t1.stop(),t1.reset() //сброс при переключении схемы
	ex.style.display ='none';// убрать кнопку рк при переключении
 }
 
//динамическая функция кнопок управленя работой анимации
//Приклике пуск убирает отрисовку по позициям
function playHv(){
	  for(let i=0; i < pk.length; i++){ // выбирает весь класс рк
	  pk[i].style.display="none"; // и убирает предыдущую отрисовку
	}
	for(let i=0; i < pp.length; i++){ // выбирает весь класс рp
	  pp[i].style.display="none"; // и убирает предыдущую отрисовку
	}



    let a;
let	s = document.querySelector('#speed').value; //скорость анимации
if(s >= 0 && s <=12){
    a = 0;
}
if(s >= 13 && s <= 36){
     a = 2.5;
}
if(s >= 37 && s <= 62){
     a = 5;
}
if(s >= 63 && s <= 82){
     a = 7.5;
}
if(s >= 83 && s <= 100){
     a = 10;
}
    r.play(a)
		
	};


function stopHv(){
    r.stop();
}

function resetHv(){
	r.reset(), r.stop();
}

